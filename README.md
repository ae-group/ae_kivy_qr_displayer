<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project ae.ae V0.3.95 -->
<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project aedev.tpl_namespace_root V0.3.14 -->
# kivy_qr_displayer 0.3.10

[![GitLab develop](https://img.shields.io/gitlab/pipeline/ae-group/ae_kivy_qr_displayer/develop?logo=python)](
    https://gitlab.com/ae-group/ae_kivy_qr_displayer)
[![LatestPyPIrelease](
    https://img.shields.io/gitlab/pipeline/ae-group/ae_kivy_qr_displayer/release0.3.9?logo=python)](
    https://gitlab.com/ae-group/ae_kivy_qr_displayer/-/tree/release0.3.9)
[![PyPIVersions](https://img.shields.io/pypi/v/ae_kivy_qr_displayer)](
    https://pypi.org/project/ae-kivy-qr-displayer/#history)

>ae_kivy_qr_displayer package 0.3.10.

[![Coverage](https://ae-group.gitlab.io/ae_kivy_qr_displayer/coverage.svg)](
    https://ae-group.gitlab.io/ae_kivy_qr_displayer/coverage/index.html)
[![MyPyPrecision](https://ae-group.gitlab.io/ae_kivy_qr_displayer/mypy.svg)](
    https://ae-group.gitlab.io/ae_kivy_qr_displayer/lineprecision.txt)
[![PyLintScore](https://ae-group.gitlab.io/ae_kivy_qr_displayer/pylint.svg)](
    https://ae-group.gitlab.io/ae_kivy_qr_displayer/pylint.log)

[![PyPIImplementation](https://img.shields.io/pypi/implementation/ae_kivy_qr_displayer)](
    https://gitlab.com/ae-group/ae_kivy_qr_displayer/)
[![PyPIPyVersions](https://img.shields.io/pypi/pyversions/ae_kivy_qr_displayer)](
    https://gitlab.com/ae-group/ae_kivy_qr_displayer/)
[![PyPIWheel](https://img.shields.io/pypi/wheel/ae_kivy_qr_displayer)](
    https://gitlab.com/ae-group/ae_kivy_qr_displayer/)
[![PyPIFormat](https://img.shields.io/pypi/format/ae_kivy_qr_displayer)](
    https://pypi.org/project/ae-kivy-qr-displayer/)
[![PyPILicense](https://img.shields.io/pypi/l/ae_kivy_qr_displayer)](
    https://gitlab.com/ae-group/ae_kivy_qr_displayer/-/blob/develop/LICENSE.md)
[![PyPIStatus](https://img.shields.io/pypi/status/ae_kivy_qr_displayer)](
    https://libraries.io/pypi/ae-kivy-qr-displayer)
[![PyPIDownloads](https://img.shields.io/pypi/dm/ae_kivy_qr_displayer)](
    https://pypi.org/project/ae-kivy-qr-displayer/#files)


## installation


execute the following command to install the
ae.kivy_qr_displayer package
in the currently active virtual environment:
 
```shell script
pip install ae-kivy-qr-displayer
```

if you want to contribute to this portion then first fork
[the ae_kivy_qr_displayer repository at GitLab](
https://gitlab.com/ae-group/ae_kivy_qr_displayer "ae.kivy_qr_displayer code repository").
after that pull it to your machine and finally execute the
following command in the root folder of this repository
(ae_kivy_qr_displayer):

```shell script
pip install -e .[dev]
```

the last command will install this package portion, along with the tools you need
to develop and run tests or to extend the portion documentation. to contribute only to the unit tests or to the
documentation of this portion, replace the setup extras key `dev` in the above command with `tests` or `docs`
respectively.

more detailed explanations on how to contribute to this project
[are available here](
https://gitlab.com/ae-group/ae_kivy_qr_displayer/-/blob/develop/CONTRIBUTING.rst)


## namespace portion documentation

information on the features and usage of this portion are available at
[ReadTheDocs](
https://ae.readthedocs.io/en/latest/_autosummary/ae.kivy_qr_displayer.html
"ae_kivy_qr_displayer documentation").
